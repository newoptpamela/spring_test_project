package org.example;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ContextConfiguration(classes = Application.class)
public class SavedStringRepositoryTest {

    @Autowired
    private SavedStringRepository repository;

    @Test
    public void testSaveAndRetrieve() {
        SavedString savedString = new SavedString();
        savedString.setText("test data");
        SavedString saved = repository.save(savedString);

        Optional<SavedString> found = repository.findById(saved.getId());
        assertTrue(found.isPresent());
        assertEquals("test data", found.get().getText());
    }
}

