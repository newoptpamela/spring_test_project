package org.example;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SavedStringRepository extends JpaRepository<SavedString, Long> {
}
