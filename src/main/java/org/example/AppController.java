package org.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class AppController {

    @Autowired
    private SavedStringRepository repository;

    @Value("${text.max-length}")
    private int maxLength;

    @GetMapping("/")
    public String redirectToAppTitle() {
        return "redirect:/app_title";
    }

    @GetMapping("/app_title")
    public String showPage(Model model) {
        model.addAttribute("newString", new SavedString());
        model.addAttribute("savedStrings", repository.findAll());
        return "index";
    }

    @PostMapping("/app_title")
    public String saveString(@ModelAttribute("newString") SavedString newString, Model model) {
        String text = newString.getText();
        if (text == null || text.isBlank()) {
            model.addAttribute("error", "Text cannot be empty");
        } else if (text.length() > maxLength) {
            model.addAttribute("error", "Text exceeds maximum length of " + maxLength);
        } else {
            repository.save(newString);
            model.addAttribute("success", "Text saved successfully");
        }
        model.addAttribute("newString", new SavedString());
        model.addAttribute("savedStrings", repository.findAll());
        return "index";
    }
}
